#!/usr/bin/python

# gen_graph.py <dim_graph> <num_edges> <file_name> <directory> <if_prob>

import sys
import random

Nnodes = int(sys.argv[1])
maxEdge = int(sys.argv[2])
ifprob = int(sys.argv[5])
bidir = int(sys.argv[6])
wclass = int(sys.argv[7])

m0 = 0 # numero nodi iniziale
t = 0 # istante di tempo
m = 0.0 # numero nodi
nodes = [0.0] * Nnodes
ki = [0.0] * Nnodes
probs = [0.0] * Nnodes

const = False

fkb = open(sys.argv[4] + "/kb_" + sys.argv[3] + ".pl","w")
fgr = open(sys.argv[4] + "/gr_" + sys.argv[3] + ".pl","w")


fkb.write("\
:-use_module('../../../sphere.pl').\n\
\n\
:-sphere.\n\
\n\
:- begin_phkb.\n\
\n\
\n\
subClassOf(loner,ineffective).\n")
if wclass == 1:
	fkb.write("subClassOf(someValuesFrom(connected,'Thing'),social).\n")
else:
	fkb.write("subClassOf(someValuesFrom(connected,influencer),social).\n")
fkb.write("\n\
loner(X) :- \+ social(X),person(X).\n\
discount(X) :- \+ ineffective(X),person(X).\n\
\n\
connected(X,Y):- edge(X,Y).\n\
connected(X,Y):- connected(X,Z),edge(Z,Y).\n")
if bidir == 1:
	fkb.write("edge(X,Y) :- edge(Y,X).\n")
fkb.write("\n\
person(bill).\n\
\n")
if ifprob == 1:
	fkb.write("edge(bill," + str(Nnodes) + "):" + str(random.random()) + ".\n" )
else:
	fkb.write("edge(bill," + str(Nnodes) + ").\n")
fkb.write("person(1).\n\
")
fgr.write("\
loner(?X) :- not social(?X),person(?X).\n\
discount(?X) :- not ineffective(?X),person(?X).\n\
connected(?X,?Y):- edge(?X,?Y).\n\
connected(?X,?Y):- connected(?X,?Z),edge(?Z,?Y).\n" +
# "edge(?X,?Y):-edge(?Y,?X).\n" +
"person(bill).\n\
edge(bill," + str(Nnodes) + ").\n\
person(1).\n\
")

m = m0
for t in range(1,(Nnodes + 1)):
	nei = min((t-1),maxEdge) # number of edge at iteration i
	nea = 0 # number of edges added for t
	m = m0 + t
	nodes[m - 1] = float(t)
	pi = 0.0
	for e in range(0,m):
		if const:
			if t == 1:
				ki[e] = 1.0
			else:
				ki[e] = 1.0 / (m0 + t - 1)
		else:
			if nodes[e] == 0:
				ki[e] = m
			else:
				ki[e] = m * pow((t / nodes[e]),0.5)
		pi = pi + ki[e]
	for e in range(0,m):
		probs[e] = (ki[e] / pi)
	# print nodes
	# print ki
	# print probs
	# print '='
	nai = 0
	nailist = [-1] * nei
	e = 0
	while e < nei:
		for f in range(1,m):
			if f not in nailist:
				p = random.random()
				# print str(p) + " - " + str(probs[f - 1])
				if p <= probs[f - 1]:
					if ifprob == 1:
						fkb.write('edge(' + str(t) + ',' + str(f) + '):' + str(random.random()) + '.\n')
					else:
						fkb.write('edge(' + str(t) + ',' + str(f) + ').\n')
					fgr.write('edge(' + str(t) + ',' + str(f) + ').\n')
					nea = nea + 1
					nai = nai + 1
					nailist[e] = f
					e = e + 1
					break
inf = random.randint(1,Nnodes)
fkb.write("influencer("+str(inf)+").\n")
fgr.write("influencer("+str(inf)+").\n")
fkb.write(":- end_phkb.\n")

fkb.close()
fgr.close()
		
