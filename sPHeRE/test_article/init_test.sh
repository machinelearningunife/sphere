#!/bin/bash

START=500 # Starting size
STOP=1000 # Max size
STEP=100  # Step for size

NKB=10	  # Number of different KB

BIDIR=0   # Bidirectional edges? 1: Yes - 0: No
CLASS=0   # Which class for social? 1: someValuesFrom(connected,'Thing') - 0: someValuesFrom(connected,influencer)


echo "Creating folders..."
mkdir graphs
cd graphs

for i in $(eval echo {$START..$STOP..$STEP}); do 
    mkdir $i
done
cd ..

echo "Creating kbs..."
for j in $(eval echo {$START..$STOP..$STEP}); do
	echo $j
	for i in $(eval echo {1..$NKB}); do
    	#echo $i
    	./gen_graph.py $j 2 $i graphs/$j 0 $BIDIR $CLASS
    done
done

