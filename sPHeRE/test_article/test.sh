#!/bin/bash

START=500 # Starting size
STOP=1000 # Max size
STEP=100  # Step for size

NKB=10	  # Number of different KB

# Name of output file for SWI Prolog
FILE="no_prob_bidir_no_check"
# Name of file with collected average time
FILETIME="no_prob_bidir_no_check_time"


> $FILE.log
> err.log

echo "Running test..."
for N in $(eval echo {$START..$STOP..$STEP}); do
	for I in $(eval echo {1..$NKB..1}); do
		echo $N-$I
		./run $N $I >> $FILE.log
	done
done

echo "Collecting runtime..."
./collect_time.py $FILE.log $FILETIME.txt $NKB
