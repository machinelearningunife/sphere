#!/usr/bin/python

import sys


fin=sys.argv[1]
fout=sys.argv[2]
nkb=int(sys.argv[3])

inf=open(fin,"r")
lines = inf.readlines()
inf.close()

tinit=0
tinfe=0
ttot=0
dim=2
count=0
c=0

fout=open(fout,"w")
#fout.write("Dim;T Init;T Inference;T Tot\n")

for line in lines:
	if c==0:
		c = c +1
		count = count + 1
	elif c==1:
		tinit = tinit + float(line)
		c = c+1
		print line
	elif c==2:
		c = 0
	if count == (nkb + 1):
		count = 1
		#fout.write(str(dim) + ";" + str((tinit/nkb)) +";" + str((tinfe/nkb)) + ";" + str((ttot/nkb)) + "\n")
		fout.write(str((tinit/nkb)) + "\n")
		dim = dim + 2
		print "\n\n"
		c = 1
		tinit=0

if count == nkb:
	#fout.write(str(dim) + ";" + str((tinit/(count))) +";" + str((tinfe/(count))) + ";" + str((ttot/(count))) + ";" + str((count)) + "\n")
	fout.write(str((tinit/(count))))
else:
	#fout.write(str(dim) + ";" + str((tinit/(count - 1))) +";" + str((tinfe/(count - 1))) + ";" + str((ttot/(count - 1))) + ";" + str((count - 1)) + "\n")
	fout.write(str((tinit/(count - 1))))
	
fout.close()
