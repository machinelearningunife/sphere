

:-module(trill_test,
  [close_to/2,close_to/3,run/1,is_true/1,is_false/1]).

:- meta_predicate run(:).
:- meta_predicate is_true(:).
:- meta_predicate is_false(:).

run(M:H):-
	copy_term(H,NH),
	numbervars(NH),
%	NH=(_Query,close_to('P',_Prob)),
	format("~p.~n",[NH]),
	(H=(G,R)),
	time(call(M:G)),!,
	format("\t~p.~n~n",[G]),
	call(R).

is_true(M:H):-
	copy_term(H,NH),
	numbervars(NH),
%	NH=(_Query,close_to('P',_Prob)),
	format("is true ~p.~n",[NH]),
	(H=(G)),
	time(call(M:G)),!,
	format("\t~p.~n~n",[G]).

is_false(M:H):-
	copy_term(H,NH),
	numbervars(NH),
%	NH=(_Query,close_to('P',_Prob)),
	format("is false ~p.~n",[NH]),
	(H=(G)),
	time((\+call(M:G))),!,
	format("\t~p.~n~n",[G]).

epsilon(0.09).

close_to(V,T):-
	epsilon(E),
	TLow is T-E,
	THigh is T+E,
	TLow<V,
	V<THigh.

close_to(V,T,E):-
	TLow is T-E,
	THigh is T+E,
	TLow<V,
	V<THigh.
