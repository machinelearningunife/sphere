:- module(test_ex6,
  [test_ex6/0]).
:- use_module(library(plunit)).

test_ex6:-
  run_tests([example_6]).

:- use_module(sphere_test).

:- begin_tests(example_6, []).

:- ensure_loaded('../../examples/Example_6.pl').


test(commjbq):-
  is_false((query(a(i1)))).
test(persjbq):-
  is_true((query(b(i1)))).
  
:- end_tests(example_6).
