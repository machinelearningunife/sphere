
:- format(user_error,
	  'sphere test suite.  To run all tests run ?- test.~n~n', []).

test:-
  use_module(test_ex2),
  test_ex2,
  unload_file(test_ex2),
  use_module(test_ex1),
  test_ex1,
  unload_file(test_ex1),
  %use_module(test_ex4),
  %test_ex4,
  %unload_file(test_ex4),
  use_module(test_ex6),
  test_ex6.
  %unload_file(test_ex6),
  %use_module(test_ex7),
  %test_ex7.
