:- module(test_ex2,
  [test_ex2/0]).
:- use_module(library(plunit)).

test_ex2:-
  run_tests([example_2]).

:- use_module(sphere_test).

:- begin_tests(example_2, []).

:- ensure_loaded('../../examples/Example_2.pl').

test(discbbq):-
  is_true((query(discount(bill)))).

:- end_tests(example_2).

