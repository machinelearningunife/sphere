:- module(test_ex7,
  [test_ex7/0]).
:- use_module(library(plunit)).

test_ex7:-
  run_tests([example_7]).

:- use_module(sphere_test).

:- begin_tests(example_7, []).

:- ensure_loaded('../../examples/Example_7.pl').


test(commjbq):-
  is_true((query(h(a)))).
test(persjbq):-
  is_false((query(c(a)))).
  
:- end_tests(example_7).
