:- module(test_ex4,
  [test_ex4/0]).
:- use_module(library(plunit)).

test_ex4:-
  run_tests([example_4]).

:- use_module(sphere_test).

:- begin_tests(example_4, []).

:- ensure_loaded('../../examples/Example_4.pl').


test(commjbq):-
  is_true((query(commander(john)))).
test(persjbq):-
  is_true((query(person(john)))).
  
:- end_tests(example_4).
