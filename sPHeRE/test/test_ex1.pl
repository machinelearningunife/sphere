:- module(test_ex1,
  [test_ex1/0]).
:- use_module(library(plunit)).

test_ex1:-
  run_tests([example_1]).

:- use_module(sphere_test).

:- begin_tests(example_1, []).

:- ensure_loaded('../../examples/Example_1.pl').


test(pabq):-
  is_true((query(p(a)))).
test(pbbq):-
  is_true((query(p(b)))).
test(eabq):-
  is_true((query(e(a)))).
test(ebbq):-
  is_true((query(e(b)))).
test(dabq):-
  is_false((query(d(a)))).
test(dbbq):-
  is_true((query(d(b)))).
  
:- end_tests(example_1).

