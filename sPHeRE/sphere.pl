:- module(sphere,[
      query/1,
      % get_bdd/4,
      set_sphere/2,setting_sphere/2,
      get_var_n/6,
      % op(600,fx,'?'),
      op(600,xfy,'::')
    ]).
/** <module> sphere

This module performs reasoning over Probabilistic Hybrid Knowledge Bases.
It reads probabilistic HKB and computes the probability of queries.


@author Riccardo Zese, Fabrizio Riguzzi
@license Artistic License 2.0 https://opensource.org/licenses/Artistic-2.0
@copyright Riccardo Zese, Fabrizio Riguzzi
*/
:- reexport(library(cplint_util)).
:- reexport(library(bddem)).

% :- prolog_debug(chk_secure).

:-meta_predicate query(:).
:-meta_predicate prob(:,-).
:-meta_predicate prob_meta(:,-).
:-meta_predicate bdd_dot_file(:,+,-).
:-meta_predicate bdd_dot_string(:,-,-).
:-meta_predicate get_p(:,+,-).
:-meta_predicate get_node(:,+,-).
:-meta_predicate set_sphere(:,+).
:-meta_predicate setting_sphere(:,-).
:-meta_predicate set_sw(:,+).

% :- dynamic utility/2.

:-use_module(library(lists)).
:-use_module(library(apply)).
:-use_module(library(assoc)).

:- use_module('./utility/trill.pl').

:- style_check(-discontiguous).

:- thread_local rule_n/1,goal_n/1,sphere_input_mod/1,local_sphere_setting/2,lp_axioms_for_oracle/2.

:- table oracle_call(_,_,_,_,lattice(orc/3)), oracle_call(_,_,_,_,_,lattice(orc/3)), sphere_not(_,_,_,_,_,lattice(orc/3)),sphere_neg_H(_,_,_,_,_,lattice(orc/3)),sphere_not_dl(_,_,_,_,lattice(orc/3)),sphere_not_lp(_,_,_,_,lattice(orc/3)).%, sphere_check_inconsistency/4.

prolog:message(inconsistenthkb) -->
  [ 'Inconsistent HKB' ].


default_setting_sphere(epsilon_parsing, 1e-5).
/* on, off */

%default_setting_sphere(bagof,false).
/* values: false, intermediate, all, extra */

%default_setting_sphere(compiling,off).

:-set_prolog_flag(unknown,warning).

default_setting_sphere(depth_bound,false).  %if true, it limits the derivation of the example to the value of 'depth'
default_setting_sphere(depth,5).
default_setting_sphere(single_var,false). %false:1 variable for every grounding of a rule; true: 1 variable for rule (even if a rule has more groundings),simpler.

default_setting_sphere(check_incons,false). %false the check is not performed.

default_setting_sphere(tabling,auto).
/* values:
  auto
  explicit
*/
% default_setting_sphere(prism_memoization,false). %false: original prism semantics, true: semantics with memoization


/**
 * prob_meta(:Query:atom,-Probability:float) is nondet
 *
 * To be used in place of prob/2 for meta calls (doesn't abolish tables)
 */
prob_meta(M:Goal,P):-
  term_variables(Goal,VG),
  get_next_goal_number(M,GN),
  atomic_concat('$goal',GN,NewGoal),
  Goal1=..[NewGoal|VG],
  list2and(GoalL,Goal),
  ( M:local_sphere_setting(depth_bound,true) *->
      ( process_body_db(GoalL,BDD,BDDAnd,DB,[],_Vars,BodyList2,Env,M),
        add_bdd_arg_db(Goal1,Env,BDDAnd,DB,M,Head1),
        dyn_goals(M,GoalL),
        BodyOracle = (sphere:oracle_call(Goal,M,Env,0,DB,BDDAnd))
      )
    ;
      ( process_body(GoalL,BDD,BDDAnd,[],_Vars,BodyList2,Env,M),
        add_bdd_arg(Goal1,Env,BDDAnd,M,Head1),
        dyn_goals(M,GoalL),
        BodyOracle = (sphere:oracle_call(Goal,M,Env,0,BDDAnd))
      )
  ),
  append([onec(Env,BDD)],BodyList2,BodyList3),
  list2and(BodyList3,Body2),
  M:(asserta((Head1 :- BodyOracle),RefOC)),
  M:(asserta((Head1 :- Body2),RefSC)),
  init(Env),
  check_inconsistency_activation(M),
  build_and_expand(_),
  findall((Goal,P),get_p(M:Goal1,Env,P),L),
  end(Env),
  erase(RefOC),
  erase(RefSC),
  member((Goal,P),L).

/**
 * query_meta(:Query:atom) is nondet
 *
 * To be used in place of query/1 for meta calls (doesn't abolish tables)
 */
query_meta(M:Goal):-
  term_variables(Goal,VG),
  get_next_goal_number(M,GN),
  atomic_concat('$goal',GN,NewGoal),
  Goal1=..[NewGoal|VG],
  list2and(GoalL,Goal),
  ( M:local_sphere_setting(depth_bound,true) *->
      ( process_body_db(GoalL,BDD,BDDAnd,DB,[],_Vars,BodyList2,Env,M),
        add_bdd_arg_db(Goal1,Env,BDDAnd,DB,M,Head1),
        dyn_goals(M,GoalL),
        BodyOracle = (sphere:oracle_call(Goal,M,Env,0,DB,BDDAnd))
      )
    ;
      ( process_body(GoalL,BDD,BDDAnd,[],_Vars,BodyList2,Env,M),
        add_bdd_arg(Goal1,Env,BDDAnd,M,Head1),
        dyn_goals(M,GoalL),
        BodyOracle = (sphere:oracle_call(Goal,M,Env,0,BDDAnd))
      )
  ),
  append([onec(Env,BDD)],BodyList2,BodyList3),
  list2and(BodyList3,Body2),
  M:(asserta((Head1 :- BodyOracle),RefOC)),
  M:(asserta((Head1 :- Body2),RefSC)),
  init(Env),
  check_inconsistency_activation(M),
  build_and_expand(_),
  get_query_truth(M:Goal1,Env),!,
  end(Env),
  erase(RefOC),
  erase(RefSC).



/**
 * bdd_dot_file(:Query:atom,+FileName:string,-LV:list) is det
 *
 * The predicate builds the BDD for Query and writes its dot representation
 * to file FileName and a list in LV with the association of variables to rules.
 * LV is a list of list, each sublist has three elements:
 * the multivalued variable number,
 * the rule number and the grounding substitution.
 */
bdd_dot_file(M:Goal,File,LV):-
  abolish_all_tables,
  init(Env),
  get_node(M:Goal,Env,Out),
  Out=(_,BDD),!,
  findall([V,R,S],M:v(R,S,V),LV),
  create_dot(Env,BDD,File),
  end(Env).

/**
 * bdd_dot_string(:Query:atom,-DotString:string,-LV:list) is det
 *
 * The predicate builds the BDD for Query and returns its dot representation
 * in DotString and a list in LV with the association of variables to rules.
 * LV is a list of list, each sublist has three elements:
 * the multivalued variable number,
 * the rule number and the grounding substitution.
 */
bdd_dot_string(M:Goal,dot(Dot),LV):-
  abolish_all_tables,
  init(Env),
  get_node(M:Goal,Env,Out),
  Out=(_,BDD),!,
  findall([V,R,S],M:v(R,S,V),LV),
  create_dot_string(Env,BDD,Dot),
  end(Env).




/**
 * prob(:Query:atom,-Probability:float) is nondet
 *
 * The predicate computes the probability of Query
 * If Query is not ground, it returns in backtracking all ground
 * instantiations of
 * Query together with their probabilities
 */
prob(M:Goal,P):-
  abolish_all_tables,
  prob_meta(M:Goal,P).


query(M:Goal):-
  abolish_all_tables,
  query_meta(M:Goal).

get_p(M:Goal,Env,P):-
  get_node(M:Goal,Env,BDD),
  ret_probc(Env,BDD,P).

get_query_truth(M:Goal,Env):-
    M:local_sphere_setting(depth_bound,true),!,
    M:local_sphere_setting(depth,DB),
    retractall(M:v(_,_,_)),
    retractall(M:av(_,_,_)),
    %retractall(M:dec(_,_,_)),
    add_bdd_arg_db(Goal,Env,BDD,DB,M,Goal1),%DB=depth bound
    (bagof(BDD,M:Goal1,L)*->
      or_listc(L,Env,B)
    ;
      zeroc(Env,B)
    ),
    \+ zeroc(Env,B).
  
get_query_truth(M:Goal,Env):- %with DB=false
  retractall(M:v(_,_,_)),
  retractall(M:av(_,_,_)),
  %retractall(M:dec(_,_,_)),
  add_bdd_arg(Goal,Env,BDD,M,Goal1),
  (bagof(BDD,M:Goal1,L)*->
    or_listc(L,Env,B)
  ;
    zeroc(Env,B)
    % format("-------------------------Failed goal: ~w ~n",[M:Goal])
  ),
  \+ zeroc(Env,B).

get_node(M:Goal,Env,B):-
  M:local_sphere_setting(depth_bound,true),!,
  M:local_sphere_setting(depth,DB),
  retractall(M:v(_,_,_)),
  retractall(M:av(_,_,_)),
  %retractall(M:dec(_,_,_)),
  add_bdd_arg_db(Goal,Env,BDD,DB,M,Goal1),%DB=depth bound
  (bagof(BDD,M:Goal1,L)*->
    or_listc(L,Env,B)
  ;
    zeroc(Env,B)
  ).

get_node(M:Goal,Env,B):- %with DB=false
  retractall(M:v(_,_,_)),
  retractall(M:av(_,_,_)),
  %retractall(M:dec(_,_,_)),
  add_bdd_arg(Goal,Env,BDD,M,Goal1),
  (bagof(BDD,M:Goal1,L)*->
    or_listc(L,Env,B)
  ;
    zeroc(Env,B)
    % format("-------------------------Failed goal: ~w ~n",[M:Goal])
  ).








get_next_goal_number(PName,R):-
  retract(PName:goal_n(R)),
  R1 is R+1,
  assert(PName:goal_n(R1)).


get_next_rule_number(PName,R):-
  retract(PName:rule_n(R)),
  R1 is R+1,
  assert(PName:rule_n(R1)).


assert_all([],_M,[]).

assert_all([H|T],M,[HRef|TRef]):-
  assertz(M:H,HRef),
  assert_all(T,M,TRef).


retract_all([]):-!.

retract_all([H|T]):-
  erase(H),
  retract_all(T).

/**
 * get_var_n(++M:atomic,++Environment:int,++Rule:int,++Substitution:term,++Probabilities:list,-Variable:int) is det
 *
 * Returns the index Variable of the random variable associated to rule with
 * index Rule, grounding substitution Substitution and head distribution
 * Probabilities in environment Environment.
 */
get_var_n(M,Env,R,S,Probs0,V):-
  M:query_rule(R,_H,_B,_S),!,
  (ground(Probs0)->
    maplist(is,Probs,Probs0),
    (M:v(R,S,V)->
      true
    ;
      add_query_var(Env,Probs,R,V),
      assert(M:v(R,S,V))
    )
  ;
    throw(error('Non ground probabilities not instantiated by the body'))
  ).

get_var_n(M,Env,R,S,Probs0,V):-
  (ground(Probs0)->
    maplist(is,Probs,Probs0),
    (M:v(R,S,V)->
      true
    ;
      % format("P: ~w ~w ~n",[Probs,R]),
      add_var(Env,Probs,R,V),
      assert(M:v(R,S,V))
    )
  ;
    throw(error('Non ground probabilities not instantiated by the body'))
  ).


add_bdd_arg(M:A,Env,BDD,M:A1):-
  A=..[P|Args],
  append(Args,[Env,BDD],Args1),
  A1=..[P|Args1].


add_bdd_arg_db(M:A,Env,BDD,DB,M:A1):-
  A=..[P|Args],
  append(Args,[Env,DB,BDD],Args1),
  A1=..[P|Args1].


add_bdd_arg(A,Env,BDD,_Module,A1):-
  A=..[P|Args],
  append(Args,[Env,BDD],Args1),
  A1=..[P|Args1].


add_bdd_arg_db(A,Env,BDD,DB,_Module,A1):-
  A=..[P|Args],
  append(Args,[Env,DB,BDD],Args1),
  A1=..[P|Args1].

  

dyn_goals(M,L):-
  M:lp_axioms_for_oracle(LPList0,_LBody0),
  sort(LPList0,LPList1),
  clean_lplist(LPList1,LPList),
  dyn_goals(M,L,LPList).
  %retractall(M:lp_axioms_for_oracle(_,_)).

dyn_goals(_,[],_):-!.

dyn_goals(M,[H|T],LPList):-
  functor(H,P,A),
  \+ member((P/A),LPList),!,
  (M:local_sphere_setting(depth_bound,true)->
    AD is A + 3
  ;
    AD is A + 2
  ),
  M:(dynamic P/AD),
  dyn_goals(M,T,LPList).

dyn_goals(M,[_H|T],LPList):-
  dyn_goals(M,T,LPList).


generate_rules_fact([],_Env,_VC,_R,_Probs,_N,[],_Module,_Doubled).

generate_rules_fact([Head:_P1,'':_P2],Env,VC,R,Probs,N,[Clause],Module,Doubled):-!,
  add_bdd_arg(Head,Env,BDD,Module,Head1),
  ( Doubled=true ->
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDDAnd),sphere:sphere_neg_H(Module,_H1,Head,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
    ;
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
  ).

generate_rules_fact([Head:_P|T],Env,VC,R,Probs,N,[Clause|Clauses],Module,Doubled):-
  add_bdd_arg(Head,Env,BDD,Module,Head1),
  ( Doubled=true ->
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDDAnd),sphere:sphere_neg_H(Module,_H1,Head,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
    ;
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
  ),
  N1 is N+1,
  generate_rules_fact(T,Env,VC,R,Probs,N1,Clauses,Module,Doubled).


generate_rules_fact_db([],_Env,_VC,_R,_Probs,_N,[],_Module,_Doubled).

generate_rules_fact_db([Head:_P1,'':_P2],Env,VC,R,Probs,N,[Clause],Module,Doubled):-
  add_bdd_arg_db(Head,Env,BDD,_DB,Module,Head1),
  ( Doubled=true ->
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDDAnd),sphere:sphere_neg_H(Module,_H1,Head,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
    ;
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
  ).

generate_rules_fact_db([Head:_P|T],Env,VC,R,Probs,N,[Clause|Clauses],Module,Doubled):-
  add_bdd_arg_db(Head,Env,BDD,_DB,Module,Head1),
  ( Doubled=true ->
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDDAnd),sphere:sphere_neg_H(Module,_H1,Head,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
    ;
      Clause=(Head1:-(get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,BDD),sphere:sphere_check_inconsistency(Module,Head,Env,BDD)))
  ),
  N1 is N+1,
  generate_rules_fact_db(T,Env,VC,R,Probs,N1,Clauses,Module,Doubled).


generate_clause(Head,Env,Body,VC,R,Probs,BDDAnd,N,Clause,Module):-
  add_bdd_arg(Head,Env,BDD,Module,Head1),
  Clause=(Head1:-(Body,get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,B),andc(Env,BDDAnd,B,BDD))).


generate_clause_db(Head,Env,Body,VC,R,Probs,DB,BDDAnd,N,Clause,Module):-
  add_bdd_arg_db(Head,Env,BDD,DBH,Module,Head1),
  Clause=(Head1:-(DBH>=1,DB is DBH-1,Body,get_var_n(Module,Env,R,VC,Probs,V),equalityc(Env,V,N,B),andc(Env,BDDAnd,B,BDD))).


generate_rules([],_Env,_Body,_VC,_R,_Probs,_BDDAnd,_N,[],_Module).

generate_rules([Head:_P1,'':_P2],Env,Body,VC,R,Probs,BDDAnd,N,[Clause],Module):-!,
  generate_clause(Head,Env,Body,VC,R,Probs,BDDAnd,N,Clause,Module).

generate_rules([Head:_P|T],Env,Body,VC,R,Probs,BDDAnd,N,[Clause|Clauses],Module):-
  generate_clause(Head,Env,Body,VC,R,Probs,BDDAnd,N,Clause,Module),
  N1 is N+1,
  generate_rules(T,Env,Body,VC,R,Probs,BDDAnd,N1,Clauses,Module).


generate_rules_db([],_Env,_Body,_VC,_R,_Probs,_DB,_BDDAnd,_N,[],_Module):-!.

generate_rules_db([Head:_P1,'':_P2],Env,Body,VC,R,Probs,DB,BDDAnd,N,[Clause],Module):-!,
  generate_clause_db(Head,Env,Body,VC,R,Probs,DB,BDDAnd,N,Clause,Module).

generate_rules_db([Head:_P|T],Env,Body,VC,R,Probs,DB,BDDAnd,N,[Clause|Clauses],Module):-
  generate_clause_db(Head,Env,Body,VC,R,Probs,DB,BDDAnd,N,Clause,Module),!,%agg.cut
  N1 is N+1,
  generate_rules_db(T,Env,Body,VC,R,Probs,DB,BDDAnd,N1,Clauses,Module).


process_body([],BDD,BDD,Vars,Vars,[],_Env,_Module).

process_body([\+ H|T],BDD,BDD1,Vars,Vars1,[\+ H|Rest],Env,Module):-
  builtin(H),!,
  process_body(T,BDD,BDD1,Vars,Vars1,Rest,Env,Module).

process_body([\+ db(H)|T],BDD,BDD1,Vars,Vars1,[\+ H|Rest],Env,Module):-
  !,
  process_body(T,BDD,BDD1,Vars,Vars1,Rest,Env,Module).

process_body([\+ H|T],BDD,BDD1,Vars,[BDDH,BDDN,BDD2|Vars1],
[(sphere:sphere_not(Module,H1,Pred,Doubled,Env,BDDH,BDDN)), %H1,(sphere:sphere_not(Module,H1,Pred,Doubled,Env,BDDH,BDDN)),
  andc(Env,BDD,BDDN,BDD2)|Rest],Env,Module):-!,
  add_bdd_arg(H,Env,BDDH,Module,H1),
  assert_body_pred(Module,H),
  ( name_value(H,Pred)-> Doubled=true ; (Pred=H,Doubled=false) ),
  process_body(T,BDD2,BDD1,Vars,Vars1,Rest,Env,Module).

process_body([H|T],BDD,BDD1,Vars,Vars1,[H1|Rest],Env,Module):-
  transform(H,H1),!,
  process_body(T,BDD,BDD1,Vars,Vars1,Rest,Env,Module).

process_body([H|T],BDD,BDD1,Vars,Vars1,[H|Rest],Env,Module):-
  builtin(H),!,
  process_body(T,BDD,BDD1,Vars,Vars1,Rest,Env,Module).

process_body([db(H)|T],BDD,BDD1,Vars,Vars1,[H|Rest],Env,Module):-
  !,
  process_body(T,BDD,BDD1,Vars,Vars1,Rest,Env,Module).

process_body([H|T],BDD,BDD1,Vars,[BDDH,BDD2|Vars1],
[H1,andc(Env,BDD,BDDH,BDD2)|Rest],Env,Module):-
  add_bdd_arg(H,Env,BDDH,Module,H1),
  assert_body_pred(Module,H),
  process_body(T,BDD2,BDD1,Vars,Vars1,Rest,Env,Module).

process_body_db([],BDD,BDD,_DB,Vars,Vars,[],_Env,_Module):-!.

process_body_db([\+ H|T],BDD,BDD1,DB,Vars,Vars1,[\+ H|Rest],Env,Module):-
  builtin(H),!,
  process_body_db(T,BDD,BDD1,DB,Vars,Vars1,Rest,Env,Module).

process_body_db([\+ db(H)|T],BDD,BDD1,DB,Vars,Vars1,[\+ H|Rest],Env,Module):-
  !,
  process_body_db(T,BDD,BDD1,DB,Vars,Vars1,Rest,Env,Module).

process_body_db([\+ H|T],BDD,BDD1,DB,Vars,[BDDH,BDDN,BDD2|Vars1],
[(sphere:sphere_not(Module,H1,Pred,Doubled,Env,BDDH,BDDN)), %H1,(sphere:sphere_not(Module,H1,Pred,Doubled,Env,BDDH,BDDN)),
  andc(Env,BDD,BDDN,BDD2)|Rest],Env,Module):-!,
  add_bdd_arg_db(H,Env,BDDH,DB,Module,H1),
  assert_body_pred(Module,H),
  ( name_value(H,Pred)-> Doubled=true ; (Pred=H,Doubled=false) ),
  process_body_db(T,BDD2,BDD1,DB,Vars,Vars1,Rest,Env,Module).

process_body_db([H|T],BDD,BDD1,DB,Vars,Vars1,[H1|Rest],Env,Module):-
  transform(H,H1),!,
  process_body_db(T,BDD,BDD1,DB,Vars,Vars1,Rest,Env,Module).

process_body_db([H|T],BDD,BDD1,DB,Vars,Vars1,[H|Rest],Env,Module):-
  builtin(H),!,
  process_body_db(T,BDD,BDD1,DB,Vars,Vars1,Rest,Env,Module).

process_body_db([db(H)|T],BDD,BDD1,DB,Vars,Vars1,[H|Rest],Env,Module):-
  !,
  process_body_db(T,BDD,BDD1,DB,Vars,Vars1,Rest,Env,Module).

process_body_db([H|T],BDD,BDD1,DB,Vars,[BDDH,BDD2|Vars1],
[H1,andc(Env,BDD,BDDH,BDD2)|Rest],Env,Module):-!, %agg. cut
  add_bdd_arg_db(H,Env,BDDH,DB,Module,H1),
  assert_body_pred(Module,H),
  process_body_db(T,BDD2,BDD1,DB,Vars,Vars1,Rest,Env,Module).


process_head(HeadList, GroundHeadList1) :-
  ground_prob(HeadList), !,
  process_head_ground(HeadList, 0.0, GroundHeadList),
  ( GroundHeadList = [V:P] -> 
      P1 is 1.0 - P, 
      GroundHeadList1 = [V:P,'':P1] ; 
      GroundHeadList1 = GroundHeadList
  ).

process_head(HeadList0, HeadList):-
  get_probs(HeadList0,PL),
  foldl(minus,PL,1.0,PNull),
  append(HeadList0,['':PNull],HeadList).

minus(A,B,B-A).

prob_ann(_:P0,P):-!, to_float(P0,P).
prob_ann(P0::_,P):- to_float(P0, P).

to_float(P0, P) :-
  ground(P0), !,
  P is float(P0).
to_float(P, P).

gen_head(H,P,VH,V,V1,H1:P):-copy_term((H,VH,V),(H1,VH,V1)).
gen_head_disc(H,VH,V,V1:P,H1:P1):-copy_term((H,VH,V),(H1,VH,V1)),P1 is float(P).

assert_head_pred(M,H):-
  (sphere_input_mod(M) ->
   (functor(H,Pred0,Arity),
    ((Arity==1;Arity==2) ->
      ( H=..[Pred0|Args],
        ( name_value(Pred0,Pred)-> true ; Pred=Pred0 ),
        M:lp_axioms_for_oracle(LT,LB),
        retractall(M:lp_axioms_for_oracle(_,_)),
        (dif(Args,[]) -> ADD=[Pred/Arity-Args] ; ADD=[Pred/Arity]),
        append(LT,ADD,L),
        M:assert(lp_axioms_for_oracle(L,LB))
      )
     ;
      true
    )
   )
   ;
    true
  ).

assert_body_pred(M,B):-
  (sphere_input_mod(M) ->
   (functor(B,Pred0,Arity),
    ((Arity==1;Arity==2) ->
      ( ( name_value(Pred0,Pred)-> true ; Pred=Pred0 ),
        M:lp_axioms_for_oracle(LT,LB),
        retractall(M:lp_axioms_for_oracle(_,_)),
        M:assert(lp_axioms_for_oracle(LT,[Pred/Arity|LB]))
      )
     ;
      true
    )
   )
   ;
    true
  ).

/* process_head_ground([Head:ProbHead], Prob, [Head:ProbHead|Null])
 * ----------------------------------------------------------------
 */
process_head_ground([H], Prob, [Head:ProbHead1|Null]) :-
  (H=Head:ProbHead;H=ProbHead::Head),!,
  ProbHead1 is float(ProbHead),
  ProbLast is 1.0 - Prob - ProbHead1,
  prolog_load_context(module, M),sphere_input_mod(M),
  M:local_sphere_setting(epsilon_parsing, Eps),
  EpsNeg is - Eps,
  ProbLast > EpsNeg,
  (ProbLast > Eps ->
    Null = ['':ProbLast]
  ;
    Null = []
  ),
  assert_head_pred(M,Head).

process_head_ground([H|Tail], Prob, [Head:ProbHead1|Next]) :-
  (H=Head:ProbHead;H=ProbHead::Head),
  ProbHead1 is float(ProbHead),
  ProbNext is Prob + ProbHead1,
  prolog_load_context(module, M),sphere_input_mod(M),
  assert_head_pred(M,Head),
  process_head_ground(Tail, ProbNext, Next).


ground_prob([]).

ground_prob([_Head:ProbHead|Tail]) :-!,
  ground(ProbHead), % Succeeds if there are no free variables in the term ProbHead.
  ground_prob(Tail).

ground_prob([ProbHead::_Head|Tail]) :-
  ground(ProbHead), % Succeeds if there are no free variables in the term ProbHead.
  ground_prob(Tail).


get_probs(Head, PL):-
  maplist(prob_ann,Head,PL).

/*get_probs([], []).

get_probs([_H:P|T], [P1|T1]) :-
  P1 is P,
  get_probs(T, T1).
*/

is_lp_assertion(lpClassAssertion(_,_)).
is_lp_assertion(lpPropertyAssertion(_,_,_)).


lp_assertion_to_atom(lpClassAssertion(Class,Individual),Atom):-
                Atom=..[Class,Individual].
lp_assertion_to_atom(lpPropertyAssertion(Role,Individual1, Individual2),Atom):-
                Atom=..[Role,Individual1,Individual2].

lp_assertion_to_doubledatom(lpClassAssertion(Class,Individual),Atom):-
				atomic_concat(Class,'spheredoubled',ClassD),
                Atom=..[ClassD,Individual].
lp_assertion_to_doubledatom(lpPropertyAssertion(Role,Individual1, Individual2),Atom):-
                atomic_concat(Role,'spheredoubled',RoleD),
                Atom=..[RoleD,Individual1,Individual2].


get_bdd(_M,Env,[],BDDX):- !,
  onec(Env,BDDX).

get_bdd(M,Env,[HA|T],BDDAnd):-
  get_prob_ax_mt(M,HA,AxN,H,Sub,Probs),!,
  get_var_n(M,Env,AxN,Sub,Probs,VH), 
  equalityc(Env,VH,H,BDDH),
  get_bdd(M,Env,T,BDDT),
  andc(Env,BDDH,BDDT,BDDAnd).

get_bdd(M,Env,[_H|T],BDDAnd):- !,
  onec(Env,BDDH),
  get_bdd(M,Env,T,BDDT),
  andc(Env,BDDH,BDDT,BDDAnd).

get_prob_ax_mt(M,Ax,N,0,[],[Prob,ProbN]):-
  Ax \= (_,_,_),
  compute_prob_ax(Ax,Prob),
  ProbN is 1-Prob,
  (M:na(Ax,N)-> 
    true
   ;
    (get_next_rule_number(M,N),
    assert(M:na(Ax,N)))
  ).

compute_prob_ax(Ax,Prob):-
  findall(ProbA,(owl2_model:annotationAssertion('https://sites.google.com/a/unife.it/ml/disponte#probability',Ax,literal(ProbAT)),atom_number(ProbAT,ProbA)),Probs),
  compute_prob_ax1(Probs,Prob).


compute_prob_ax1([Prob],Prob):-!.

compute_prob_ax1([Prob1,Prob2],Prob):-!,
  Prob is Prob1+Prob2-(Prob1*Prob2).

compute_prob_ax1([Prob1 | T],Prob):-
  compute_prob_ax1(T,Prob0),
  Prob is Prob1 + Prob0 - (Prob1*Prob0).


sphere_not(Module,H1,_H,_Doubled,Env,BDDH,BDDN):-
  sphere_not_lp(Module,H1,Env,BDDH,BDDN).

sphere_not(Module,_H1,H,Doubled,Env,_BDDH,BDDN):-
  sphere_not_dl(Module,H,Doubled,Env,BDDN).

sphere_check_inconsistency(Module,H,Env,BDDIn):- %gtrace, % TODO: check whether H is a Dl-atom
  Module:local_sphere_setting(check_incons,true),
  %sphere_not_dl(Module,H,0,Env,BDDH),
  H=..[Class,Individual],
  instanceOf_meta(complementOf(Class),Individual,Explanation0,Module,Env,BDDT),!,
  include(is_lp_assertion,Explanation0,LPAssertions),
  maplist(lp_assertion_to_atom,LPAssertions,Atoms),
  solve_all(Module,Atoms,Env,BDDA),
  andc(Env,BDDT,BDDA,BDDH),
  andc_inc(Env,BDDIn,BDDH,Zero),
  ( zeroc(Env,Zero) -> true ; warn_about_inconsistency(Module)).

sphere_check_inconsistency(_Module,_H,_Env,_BDDIn) :- !.

check_inconsistency_activation(M):-
  M:inconsistency_warning, !,
  retractall(M:inconsistency_warning),
  set_sphere(M:check_incons,true).

check_inconsistency_activation(_M).

warn_about_inconsistency(M) :-
  ( M:inconsistency_warning ->
      true
    ;
      ( print_message(warning,inconsistenthkb),
        assert(M:inconsistency_warning),
        set_sphere(M:check_incons,false)
      )
  ).

sphere_neg_H(Module,_H1,H,Env,_BDDIn,BDDN):- %gtrace, % TODO: check whether H is a Dl-atom
  sphere_not_dl(Module,H,0,Env,BDDH),
  bdd_notc(Env,BDDH,BDDN).


sphere_not_lp(Module,H1,Env,BDDH,BDDN):-
  call(Module:H1),
  bdd_notc(Env,BDDH,BDDN).



sphere_not_dl(M,H,Doubled,Env,BDDN):-
  H=..[Class,Individual],
  instanceOf_meta(complementOf(Class),Individual,Explanation0,M,Env,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  (Doubled=0 -> 
     maplist(lp_assertion_to_atom,LPAssertions,Atoms)
    ;
     maplist(lp_assertion_to_doubledatom,LPAssertions,Atoms)
  ),
  solve_all(M,Atoms,Env,BDDA),
  andc(Env,BDDT,BDDA,BDDN).

sphere_not_dl(_M,H,_Doubled,Env,BDDN):-
  H=..[_|Args],
  maplist(nonvar,Args),
  zeroc(Env,BDDN).

oracle_call(Head,M,Env,Doubled,BDD):-
  Head=..[Class,Individual],
  instanceOf_meta(Class,Individual,Explanation0,M,Env,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  (Doubled=0 -> 
     maplist(lp_assertion_to_atom,LPAssertions,Atoms)
    ;
     maplist(lp_assertion_to_doubledatom,LPAssertions,Atoms)
  ),
  solve_all(M,Atoms,Env,BDDA),
  andc(Env,BDDT,BDDA,BDD).

/*
oracle_call(Head,M,Env,BDD):-
  Head=..[Class,Individual],
  instanceOf_meta(complementOf(Class),Individual,Explanation0),
  get_bdd(Env,Explanation0,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  maplist(lp_assertion_to_atom,LPAssertions,Atoms),
  solve_all(Atoms,Env,BDDA),
  andc(Env,BDDT,BDDA,BDD0),
  bdd_notc(Env,BDD0,BDD).
*/

oracle_call(Head,M,Env,Doubled,BDD):-
  Head=..[Role,Individual1,Individual2],
  property_value_meta(Role,Individual1,Individual2,Explanation0,M,Env,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  (Doubled=0 -> 
     maplist(lp_assertion_to_atom,LPAssertions,Atoms)
    ;
     maplist(lp_assertion_to_doubledatom,LPAssertions,Atoms)
  ),
   solve_all(M,Atoms,Env,BDDA),
  andc(Env,BDDT,BDDA,BDD).


oracle_call(Head,_M,Env,_Doubled,BDD):-
  Head=..[_|Args],
  maplist(nonvar,Args),
  zeroc(Env,BDD).


oracle_call(Head,M,Env,Doubled,DB,BDD):-
  Head=..[Class,Individual],
  instanceOf_meta(Class,Individual,Explanation0,M,Env,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  (Doubled=0 -> 
     maplist(lp_assertion_to_atom,LPAssertions,Atoms)
    ;
     maplist(lp_assertion_to_doubledatom,LPAssertions,Atoms)
  ),
  solve_all(M,Atoms,Env,DB,BDDA),
  andc(Env,BDDT,BDDA,BDD).

/*
oracle_call(Head,M,Env,BDD):-
  Head=..[Class,Individual],
  instanceOf_meta(complementOf(Class),Individual,Explanation0),
  get_bdd(Env,Explanation0,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  maplist(lp_assertion_to_atom,LPAssertions,Atoms),
  solve_all(Atoms,Env,BDDA),
  andc(Env,BDDT,BDDA,BDD0),
  bdd_notc(Env,BDD0,BDD).
*/

oracle_call(Head,M,Env,Doubled,DB,BDD):-
  Head=..[Role,Individual1,Individual2],
  property_value_meta(Role,Individual1,Individual2,Explanation0,M,Env,BDDT),
  include(is_lp_assertion,Explanation0,LPAssertions),
  (Doubled=0 -> 
     maplist(lp_assertion_to_atom,LPAssertions,Atoms)
    ;
     maplist(lp_assertion_to_doubledatom,LPAssertions,Atoms)
  ),
  solve_all(M,Atoms,Env,DB,BDDA),
  andc(Env,BDDT,BDDA,BDD).
  
oracle_call(Head,_M,Env,_Doubled,_DB,BDD):-
  Head=..[_|Args],
  maplist(nonvar,Args),
  zeroc(Env,BDD).

solve_all(_M,[],Env,BDDOne):- !,
  onec(Env,BDDOne).

solve_all(M,[H|T],Env,BDD):-
  call(M:H,Env,BDDH),
  solve_all(M,T,Env,BDDT),
  andc(Env,BDDT,BDDH,BDD).

solve_all(_M,[],Env,_DBH,BDDOne):- !,
  onec(Env,BDDOne).

solve_all(M,[H|T],Env,DBH,BDD):-
  call(M:H,Env,DBH,BDDH),
  solve_all(M,T,Env,DBH,BDDT),
  andc(Env,BDDT,BDDH,BDD).


/**
 * set_sphere(:Parameter:atom,+Value:term) is det
 *
 * The predicate sets the value of a parameter
 * For a list of parameters see
 * https://github.com/friguzzi/cplint/blob/master/doc/manual.pdf or
 * http://ds.ing.unife.it/~friguzzi/software/cplint-swi/manual.html
 *
 */
set_sphere(M:Parameter,Value):-
  retract(M:local_sphere_setting(Parameter,_)),
  assert(M:local_sphere_setting(Parameter,Value)).

/**
 * setting_sphere(:Parameter:atom,?Value:term) is det
 *
 * The predicate returns the value of a parameter
 * For a list of parameters see
 * https://github.com/friguzzi/cplint/blob/master/doc/manual.pdf or
 * http://ds.ing.unife.it/~friguzzi/software/cplint-swi/manual.html
 */
setting_sphere(M:P,V):-
  M:local_sphere_setting(P,V).

/*
delete_equal([],_,[]).

delete_equal([H|T],E,T):-
  H == E,!.

delete_equal([H|T],E,[H|T1]):-
  delete_equal(T,E,T1).
*/

set_sw(M:A,B):-
  get_next_rule_number(M,R),
  assert(M:sw(R,A,B)).

/*
act(M,A/B):-
  (M:local_sphere_setting(depth_bound,true)->
    B1 is B + 3
  ;
    B1 is B + 2
  ),
  M:(dynamic A/B1).
*/

tab(M,A/B,P):-
  length(Args0,B),
  (M:local_sphere_setting(depth_bound,true)->
    ExtraArgs=[_,_,lattice(orc/3)]
  ;
    ExtraArgs=[_,lattice(orc/3)]
  ),
  append(Args0,ExtraArgs,Args),
  P=..[A|Args],
  PT=..[A|Args0],
  assert(M:tabled(PT)).

zero_clause(M,A/B,(H:-maplist(nonvar,Args0),zeroc(Env,BDD))):-
  length(Args0,B),
  (M:local_sphere_setting(depth_bound,true)->
    ExtraArgs=[Env,_,BDD]
  ;
    ExtraArgs=[Env,BDD]
  ),
  append(Args0,ExtraArgs,Args),
  H=..[A|Args].

assert_lp_axioms(L):-
  assert_lp_axioms(L,I),
  (dif(I,[]) -> (sort(I,Inds),owl2_model:create_and_assert_axioms(lpIndividuals,[Inds]));
    true).

assert_lp_axioms([],[]).

assert_lp_axioms([P/1|T],I):- !,
  owl2_model:create_and_assert_axioms(lpClassAssertion,[P]),
  assert_lp_axioms(T,I).

assert_lp_axioms([P/2|T],I):- !,
  owl2_model:create_and_assert_axioms(lpPropertyAssertion,[P]),
  assert_lp_axioms(T,I).

assert_lp_axioms([P/1-[A]|T],I):-!, 
  extract_ground([A],L1),
  owl2_model:create_and_assert_axioms(lpClassAssertion,[P]),
  assert_lp_axioms(T,I0),
  append(L1,I0,I).

assert_lp_axioms([P/2-L|T],I):- !,
  extract_ground(L,L1),
  owl2_model:create_and_assert_axioms(lpPropertyAssertion,[P]),
  assert_lp_axioms(T,I0),
  append(L1,I0,I).

assert_lp_axioms([_P/_A-L|T],I):-
  extract_ground(L,L1),
  assert_lp_axioms(T,I0),
  append(L1,I0,I).

extract_ground([],[]).
extract_ground([H|T],[H|T1]):-
  ground(H),!,
  extract_ground(T,T1).
extract_ground([_H|T],T1):-
  extract_ground(T,T1).


clean_lplist([],[]).

clean_lplist([P/A|T0],[P/A|T]):-
  clean_lplist(T0,T).

clean_lplist([P/A-_|T0],[P/A|T]):-
  clean_lplist(T0,T).

create_ocrules_db([],[],_).

create_ocrules_db([P/A|T0],[(H:-(DBH>=1,DB is DBH-1,sphere:oracle_call(OCQ,M,Env,0,DB,BDD))),(HD:-(DBH>=1,DB is DBH-1,sphere:oracle_call(OCQ,M,Env,1,DB,BDD)))|T],M):-
  length(Args,A),
  atomic_concat(P,'spheredoubled',PD),
  append([P|Args],[Env,DBH,BDD],H0),
  append([PD|Args],[Env,DBH,BDD],H0D),
  H=..H0,
  HD=..H0D,
  OCQ=..[P|Args],
  create_ocrules_db(T0,T,M).

create_ocrules([],[],_).

create_ocrules([P/A|T0],[(H:-(sphere:oracle_call(OCQ,M,Env,0,BDD))),(HD:-(sphere:oracle_call(OCQ,M,Env,1,BDD)))|T],M):-
  length(Args,A),
  atomic_concat(P,'spheredoubled',PD),
  append([P|Args],[Env,BDD],H0),
  append([PD|Args],[Env,BDD],H0D),
  H=..H0,
  HD=..H0D,
  OCQ=..[P|Args],
  create_ocrules(T0,T,M).

create_oracle_calls(M,LPList,LBody,OCRules):-
  M:local_sphere_setting(depth_bound,true),!,
  clean_lplist(LPList,LPListCleaned),
  append(LPListCleaned,LBody,OCList0),
  sort(OCList0,OCList),
  create_ocrules_db(OCList,OCRules,M).

create_oracle_calls(M,LPList,LBody,OCRules):-
  clean_lplist(LPList,LPListCleaned),
  append(LPListCleaned,LBody,OCList0),
  sort(OCList0,OCList),
  create_ocrules(OCList,OCRules,M).

to_table(M,Heads,[],Heads):-
  M:local_sphere_setting(tabling,explicit),!.

to_table(M,Heads,ProcTabDir,Heads1):-
  maplist(tab_dir(M),Heads,TabDirList,Heads1L),
  append(TabDirList,TabDir),
  maplist(system:term_expansion,TabDir,ProcTabDirL),
  append(ProcTabDirL,ProcTabDir),
  append(Heads1L,Heads1).

tab_dir(_M,'':_,[],[]):-!.

tab_dir(M,H:P,[],[H:P]):-
  M:tabled(H),!.

tab_dir(M,Head,[(:- table HT)],[H1:P]):-
  (Head=H:P;Head=P::H),!,
  functor(H,F,A0),
  functor(PT,F,A0),
  PT=..[F|Args0],
  (M:local_sphere_setting(depth_bound,true)->
    ExtraArgs=[_,_,lattice(orc/3)]
  ;
    ExtraArgs=[_,lattice(orc/3)]
  ),
  append(Args0,ExtraArgs,Args),
  HT=..[F|Args],
  H=..[_|ArgsH],
  H1=..[F|ArgsH],
  assert(M:tabled(PT)),
  zero_clause(M,F/A0,LZ),
  assert(M:zero_clauses(LZ)).


% ================================================================================

double_head([],[]).

double_head([H:P|T],[HD:P|TD]):-!,
  H=..[HP|Args],
  atomic_concat(HP,'spheredoubled',HPD),
  HD=..[HPD|Args],
  double_head(T,TD).

double_head([H|T],[HD|TD]):-
  H\=(_:_),H\=(_::_),
  H=..[HP|Args],
  atomic_concat(HP,'spheredoubled',HPD),
  HD=..[HPD|Args],
  double_head(T,TD).

double_body([],[],[]).

double_body([\+H|T],[\+HD|TD1],[\+H|TD2]):-!,
  H=..[HP|Args],
  atomic_concat(HP,'spheredoubled',HPD),
  HD=..[HPD|Args],
  double_body(T,TD1,TD2).

double_body([H|T],[H|TD1],[HD|TD2]):-
  H=..[HP|Args],
  atomic_concat(HP,'spheredoubled',HPD),
  HD=..[HPD|Args],
  double_body(T,TD1,TD2).


create_doubled_rules_1_db(HeadList,BodyD1,R,Probs,DB,M,[rule_by_num(R,HeadList,BodyD1,VC1)|Clauses]):-
  process_body_db(BodyD1,BDD,BDDAnd,DB,[],_Vars,BodyList1,Env,M),%aggiunge bdd pred
  append([onec(Env,BDD)],BodyList1,BodyList2),
  list2and(BodyList2,Body1),
  append(HeadList,BodyD1,List),
  term_variables(List,VC), % extracts variables from head and body
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  to_table(M,HeadList,TabDir,HeadList1), % adds table
  ( M:local_sphere_setting(check_incons,true) -> 
      generate_rules_d_1_db(HeadList1,HeadList,Env,Body1,VC1,R,Probs,DB,BDDAnd,0,Clauses0,M) %genera regoles
    ;
      generate_rules_db(HeadList1,Env,Body1,VC1,R,Probs,DB,BDDAnd,0,Clauses0,M) %genera regoles
  ),
  append(TabDir,Clauses0,Clauses).

create_doubled_rules_1(HeadList,BodyD1,R,Probs,M,[rule_by_num(R,HeadList,BodyD1,VC1)|Clauses]):-
  process_body(BodyD1,BDD,BDDAnd,[],_Vars,BodyList1,Env,M),%aggiunge bdd pred
  append([onec(Env,BDD)],BodyList1,BodyList2),
  list2and(BodyList2,Body1),
  append(HeadList,BodyD1,List),
  term_variables(List,VC), % extracts variables from head and body
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  to_table(M,HeadList,TabDir,HeadList1), % adds table
  ( M:local_sphere_setting(check_incons,true) -> 
      generate_rules_d_1(HeadList1,HeadList,Env,Body1,VC1,R,Probs,BDDAnd,0,Clauses0,M) % generates rules
    ;
      generate_rules(HeadList1,Env,Body1,VC1,R,Probs,BDDAnd,0,Clauses0,M) % generates rules
  ),
  append(TabDir,Clauses0,Clauses).

create_doubled_rules_2_db(HeadList,BodyD1,R,Probs,DB,M,[rule_by_num(R,HeadList,BodyD1,VC1)|Clauses]):-
  process_body_db(BodyD1,BDD,BDDAnd,DB,[],_Vars,BodyList1,Env,M),% adds bdd pred
  append([onec(Env,BDD)],BodyList1,BodyList2),
  list2and(BodyList2,Body1),
  append(HeadList,BodyD1,List),
  term_variables(List,VC), % extracts variables from head and body
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  to_table(M,HeadList,TabDir,HeadList1), % adds table
  generate_rules_d_2_db(HeadList1,Env,Body1,VC1,R,Probs,DB,BDDAnd,0,Clauses0,M), % generates rules
  append(TabDir,Clauses0,Clauses).

create_doubled_rules_2(HeadList,BodyD1,R,Probs,M,[rule_by_num(R,HeadList,BodyD1,VC1)|Clauses]):-
  process_body(BodyD1,BDD,BDDAnd,[],_Vars,BodyList1,Env,M),%aggiunge bdd pred
  append([onec(Env,BDD)],BodyList1,BodyList2),
  list2and(BodyList2,Body1),
  append(HeadList,BodyD1,List),
  term_variables(List,VC), % extracts variables from head and body
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  to_table(M,HeadList,TabDir,HeadList1), % adds table
  generate_rules_d_2(HeadList1,Env,Body1,VC1,R,Probs,BDDAnd,0,Clauses0,M), % generates rules
  append(TabDir,Clauses0,Clauses).

generate_rules_d_1_db([],_,_Env,_Body,_VC,_R,_Probs,_DB,_BDDAnd,_N,[],_Module).

generate_rules_d_1_db([Head:_P1,'':_P2],[HeadNT:_P1NT,'':_P2NT],Env,Body,VC,R,Probs,DB,BDDAnd,N,[Clause],Module):-!,
  list2and(BodyList,Body),
  append(BodyList,[sphere:sphere_check_inconsistency(Module,HeadNT,Env,BDDAnd)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause_db(Head,Env,BodyD,VC,R,Probs,DB,BDDAnd,N,Clause,Module).

generate_rules_d_1_db([Head:_P|T],[HeadNT:_PNT|TNT],Env,Body,VC,R,Probs,DB,BDDAnd,N,[Clause|Clauses],Module):-
  %% add test
  list2and(BodyList,Body),
  append(BodyList,[sphere:sphere_check_inconsistency(Module,HeadNT,Env,BDDAnd)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause_db(Head,Env,BodyD,VC,R,Probs,DB,BDDAnd,N,Clause,Module),
  N1 is N+1,
  generate_rules_d_1_db(T,TNT,Env,Body,VC,R,Probs,DB,BDDAnd,N1,Clauses,Module).

generate_rules_d_1([],_,_Env,_Body,_VC,_R,_Probs,_BDDAnd,_N,[],_Module).

generate_rules_d_1([Head:_P1,'':_P2],[HeadNT:_P1NT,'':_P2NT],Env,Body,VC,R,Probs,BDDAnd,N,[Clause],Module):-!,
  list2and(BodyList,Body),
  append(BodyList,[sphere:sphere_check_inconsistency(Module,HeadNT,Env,BDDAnd)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause(Head,Env,BodyD,VC,R,Probs,BDDAnd,N,Clause,Module).

generate_rules_d_1([Head:_P|T],[HeadNT:_PNT|TNT],Env,Body,VC,R,Probs,BDDAnd,N,[Clause|Clauses],Module):-
  list2and(BodyList,Body),
  append(BodyList,[sphere:sphere_check_inconsistency(Module,HeadNT,Env,BDDAnd)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause(Head,Env,BodyD,VC,R,Probs,BDDAnd,N,Clause,Module),
  N1 is N+1,
  generate_rules_d_1(T,TNT,Env,Body,VC,R,Probs,BDDAnd,N1,Clauses,Module).

generate_rules_d_2_db([],_Env,_Body,_VC,_R,_Probs,_DB,_BDDAnd,_N,[],_Module).

generate_rules_d_2_db([Head:_P1,'':_P2],Env,Body,VC,R,Probs,DB,BDDAnd,N,[Clause],Module):-!,
  list2and(BodyList,Body),name_value(Head,HeadNegH),
  append(BodyList,[sphere:sphere_neg_H(Module,_H1,HeadNegH,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(Module,HeadNegH,Env,BDDAndD)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause_2_db(Head,Env,BodyD,VC,R,Probs,DB,BDDAndD,N,Clause,Module).

generate_rules_d_2_db([Head:_P|T],Env,Body,VC,R,Probs,DB,BDDAnd,N,[Clause|Clauses],Module):-
  list2and(BodyList,Body),name_value(Head,HeadNegH),
  append(BodyList,[sphere:sphere_neg_H(Module,_H1,HeadNegH,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(Module,HeadNegH,Env,BDDAndD)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause_db(Head,Env,BodyD,VC,R,Probs,DB,BDDAndD,N,Clause,Module),
  N1 is N+1,
  generate_rules_d_2_db(T,Env,Body,VC,R,Probs,DB,BDDAnd,N1,Clauses,Module).

generate_rules_d_2([],_Env,_Body,_VC,_R,_Probs,_BDDAnd,_N,[],_Module).

generate_rules_d_2([Head:_P1,'':_P2],Env,Body,VC,R,Probs,BDDAnd,N,[Clause],Module):-!,
  list2and(BodyList,Body),name_value(Head,HeadNegH),
  append(BodyList,[sphere:sphere_neg_H(Module,_H1,HeadNegH,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(Module,HeadNegH,Env,BDDAndD)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause(Head,Env,BodyD,VC,R,Probs,BDDAndD,N,Clause,Module).

generate_rules_d_2([Head:_P|T],Env,Body,VC,R,Probs,BDDAnd,N,[Clause|Clauses],Module):-
  list2and(BodyList,Body),name_value(Head,HeadNegH),
  append(BodyList,[sphere:sphere_neg_H(Module,_H1,HeadNegH,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(Module,HeadNegH,Env,BDDAndD)],BodyListD),
  list2and(BodyListD,BodyD),
  generate_clause(Head,Env,BodyD,VC,R,Probs,BDDAndD,N,Clause,Module),
  N1 is N+1,
  generate_rules_d_2(T,Env,Body,VC,R,Probs,BDDAnd,N1,Clauses,Module).

name_value(H, H1) :-
 H=..[String|Args],
 sub_string(String, Before, _, _, "spheredoubled"), !,
 sub_string(String, 0, Before, _, NameString),
 atom_string(Name,NameString),
 H1=..[Name|Args].

%name_value(H,H).

% ================================================================================
%
%   TERM EXPANSION
%
% ================================================================================
sphere_expansion(begin_of_file,_):-
  !,
  fail.

sphere_expansion((:- begin_phkb), []) :- %gtrace,
  prolog_load_context(module, M),
  sphere_input_mod(M),!,
  assert(M:sphere_on).

sphere_expansion((:- end_phkb), OCRules) :-
  prolog_load_context(module, M),
  sphere_input_mod(M),!,
  M:lp_axioms_for_oracle(LPList0,LBody0),
  sort(LPList0,LPList),
  sort(LBody0,LBody),
  assert_lp_axioms(LPList),
  create_oracle_calls(M,LPList,LBody,OCRules),
  %retractall(M:lp_axioms_for_oracle(_,_)),
  retractall(M:sphere_on).

sphere_expansion(values(A,B), values(A,B)) :-
  prolog_load_context(module, M),
  sphere_input_mod(M),M:sphere_on,!.

sphere_expansion(kb_prefix(A,B), []) :-
  trill:add_kb_prefix(A,B),!.

sphere_expansion(owl_rdf(A), []) :-
  owl2_model:parse_rdf_from_owl_rdf_pred(A),!.

sphere_expansion((Head :- Body),Clauses):-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
% disjunctive clause with more than one head atom and depth_bound
  Head = (_;_), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),
  process_head(HeadListOrD, HeadListD),
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  get_next_rule_number(M,R),
  get_probs(HeadList,Probs),
  create_doubled_rules_1_db(HeadList,BodyListD1,R,Probs,DB,M,ClausesD1),
  create_doubled_rules_2_db(HeadListD,BodyListD2,R,Probs,DB,M,ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).


sphere_expansion((Head :- Body),Clauses):-
	  %trace,
    ((Head:- Body) \= ((sphere_expansion(_,_)) :- _ )),
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
% disjunctive clause with more than one head atom without depth_bound
  Head = (_;_), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),
  process_head(HeadListOrD, HeadListD),
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  get_next_rule_number(M,R),
  get_probs(HeadList,Probs), % gets the probs list
  create_doubled_rules_1(HeadList,BodyListD1,R,Probs,M,ClausesD1),
  create_doubled_rules_2(HeadListD,BodyListD2,R,Probs,M,ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion((Head :- Body), []) :-
% disjunctive clause with a single head atom with prob. 0 without depth_bound --> the rule is not loaded in the theory and not counted by NR
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
  (Head = (_:P);Head=(P::_)),
  ground(P),
  P=:=0.0, !.

sphere_expansion((Head :- Body), Clauses) :-
% disjunctive clause with a single head atom and depth_bound
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
  ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),
  process_head(HeadListOrD, HeadListD),
  HeadList=[_H:_],!,
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  process_body_db(BodyListD1,BDD,BDDAnd,DB,[],_Vars,BodyListD11,Env,M),
  process_body_db(BodyListD2,BDD,BDDAnd,DB,[],_VarsD,BodyListD21,Env,M),
  append([onec(Env,BDD)],BodyListD11,BodyListD12),
  append([onec(Env,BDD)],BodyListD21,BodyListD22),
  append(BodyListD12,[sphere:sphere_check_inconsistency(M,H,Env,BDDAnd)],BodyListD13),
  append(BodyListD22,[sphere:sphere_neg_H(M,_H12,H,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(M,H,Env,BDDAndD)],BodyListD23),
  list2and([DBH>=1,DB is DBH -1|BodyListD13],Body1),
  list2and([DBH>=1,DB is DBH -1|BodyListD23],BodyD1),
  to_table(M,HeadList,TabDirD1,[H1:_]),
  to_table(M,HeadListD,TabDirD2,[HD1:_]),
  add_bdd_arg_db(H1,Env,BDDAnd,DBH,M,Head1),
  add_bdd_arg_db(HD1,Env,BDDAndD,DBH,M,HeadD1),
  append(TabDirD1,[(Head1 :- Body1)],ClausesD1),
  append(TabDirD2,[(HeadD1 :- BodyD1)],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion((Head :- Body), Clauses) :-
% disjunctive clause with a single head atom without depth_bound with prob =1
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),
  process_head(HeadListOrD, HeadListD),
  HeadList=[_H:_],!,
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  process_body(BodyListD1,BDD,BDDAnd,[],_Vars,BodyListD11,Env,M),
  process_body(BodyListD2,BDD,BDDAnd,[],_VarsD,BodyListD21,Env,M),
  append([onec(Env,BDD)],BodyListD11,BodyListD12),
  append([onec(Env,BDD)],BodyListD21,BodyListD22),
  append(BodyListD12,[sphere:sphere_check_inconsistency(M,H,Env,BDDAnd)],BodyListD13),
  append(BodyListD22,[sphere:sphere_neg_H(M,_H12,H,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(M,H,Env,BDDAndD)],BodyListD23),
  list2and(BodyListD13,Body1),
  list2and(BodyListD23,BodyD1),
  to_table(M,HeadList,TabDirD1,[H1:_]),
  to_table(M,HeadListD,TabDirD2,[HD1:_]),
  add_bdd_arg(H1,Env,BDDAnd,M,Head1),
  add_bdd_arg(HD1,Env,BDDAndD,M,HeadD1),
  append(TabDirD1,[(Head1 :- Body1)],ClausesD1),
  append(TabDirD2,[(HeadD1 :- BodyD1)],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

/*
sphere_expansion((Head :- Body), Clauses) :-
% disjunctive clause with a single head atom and DB, with prob \= 1
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
  ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
  (Head = (_H:_);Head=(_::_H)), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),% adds null if needed
  process_head(HeadListOrD, HeadListD),
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  process_body_db(BodyListD1,BDD,BDDAnd, DB,[],_Vars,BodyListD11,Env,M),
  process_body_db(BodyListD2,BDD,BDDAnd, DB,[],_VarsD,BodyListD21,Env,M),
  append([onec(Env,BDD)],BodyListD11,BodyListD12),
  append([onec(Env,BDD)],BodyListD21,BodyListD22),
  append(BodyListD12,[sphere:sphere_check_inconsistency(M,H,Env,BDDAnd)],BodyListD13),
  append(BodyListD22,[sphere:sphere_neg_H(M,_H12,H,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(M,H,Env,BDDAndD)],BodyListD23),
  list2and(BodyListD13,Body1),
  list2and(BodyListD23,BodyD1),
  append(HeadList,BodyListD1,ListD1),
  append(HeadListD,BodyListD2,ListD2),
  term_variables(ListD1,VC1),
  term_variables(ListD2,VC2),
  get_next_rule_number(M,R),
  get_probs(HeadList,Probs),%***test single_var
  (M:local_sphere_setting(single_var,true)->
    VC11 = [],VC12 = []
  ;
    VC11 = VC1,VC12 = VC2
  ),
  to_table(M,HeadList,TabDir,[H1:_]),
  to_table(M,HeadListD,TabDir,[HD1:_]),
  generate_doubled_clause_1_db(H1,Env,Body1,VC11,R,Probs,DB,BDDAnd,0,Clauses0,M),
  generate_doubled_clause_2_db(HD1,Env,BodyD1,VC12,R,Probs,DB,BDDAnd,0,Clauses1,M),
  append(TabDir,[Clauses0,Clauses1],Clauses).

sphere_expansion((Head :- Body), [rule_by_num(R,HeadList,BodyListD13,VC11),rule_by_num(R,HeadListD,BodyListD23,VC12)|Clauses]) :-
% disjunctive clause with a single head atom without DB, with prob \= 1
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
  (Head = (_H:_);Head = (_::_H)), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),% adds null if needed
  process_head(HeadListOrD, HeadListD),
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  process_body(BodyListD1,BDD,BDDAnd, [],_Vars,BodyListD11,Env,M),
  process_body(BodyListD2,BDD,BDDAnd, [],_VarsD,BodyListD21,Env,M),
  append([onec(Env,BDD)],BodyListD11,BodyListD12),
  append([onec(Env,BDD)],BodyListD21,BodyListD22),
  append(BodyListD12,[sphere:sphere_check_inconsistency(M,H,Env,BDDAnd)],BodyListD13),
  append(BodyListD22,[sphere:sphere_neg_H(M,_H12,H,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(M,H,Env,BDDAndD)],BodyListD23),
  list2and(BodyListD13,Body1),
  list2and(BodyListD23,BodyD1),
  append(HeadList,BodyListD1,ListD1),
  append(HeadListD,BodyListD2,ListD2),
  term_variables(ListD1,VC1),
  term_variables(ListD2,VC2),
  get_next_rule_number(M,R),
  get_probs(HeadList,Probs),%***test single_vars
  (M:local_sphere_setting(single_var,true)->
    VC11 = [],VC12 = []
  ;
    VC11 = VC1,VC12 = VC2
  ),
  to_table(M,HeadList,TabDir,[H1:_]),
  to_table(M,HeadListD,TabDir,[HD1:_]),
  generate_doubled_clause_1(H1,Env,Body1,VC11,R,Probs,BDDAnd,0,Clauses0,M),
  generate_doubled_clause_2(HD1,Env,BodyD1,VC12,R,Probs,BDDAnd,0,Clauses1,M),
  append(TabDir,[Clauses0,Clauses1],Clauses).
*/
sphere_expansion((Head :- Body), Clauses) :-
  % disjunctive clause with a single head atom and DB, with prob \= 1
    prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
    M:local_sphere_setting(depth_bound,true),
    ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
    (Head = (_H:_);Head=(_::_H)), !,
    list2or(HeadListOr, Head),
    double_head(HeadListOr,HeadListOrD),
    process_head(HeadListOr, HeadList),% adds null if needed
    process_head(HeadListOrD, HeadListD),
    list2and(BodyList, Body),
    double_body(BodyList,BodyListD1,BodyListD2),
    get_next_rule_number(M,R),
    get_probs(HeadList,Probs),%***test single_var
    create_doubled_rules_1_db(HeadList,BodyListD1,R,Probs,DB,M,ClausesD1),
    create_doubled_rules_2_db(HeadListD,BodyListD2,R,Probs,DB,M,ClausesD2),
    append(ClausesD1,ClausesD2,Clauses).
  
  sphere_expansion((Head :- Body), Clauses) :-
  % disjunctive clause with a single head atom without DB, with prob \= 1
    prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
    ((Head:-Body) \= ((sphere_expansion(_,_) ):- _ )),
    (Head = (_H:_);Head = (_::_H)), !,
    list2or(HeadListOr, Head),
    double_head(HeadListOr,HeadListOrD),
    process_head(HeadListOr, HeadList),% adds null if needed
    process_head(HeadListOrD, HeadListD),
    list2and(BodyList, Body),
    double_body(BodyList,BodyListD1,BodyListD2),
    get_next_rule_number(M,R),
    get_probs(HeadList,Probs),%***test single_vars
    create_doubled_rules_1(HeadList,BodyListD1,R,Probs,M,ClausesD1),
    create_doubled_rules_2(HeadListD,BodyListD2,R,Probs,M,ClausesD2),
    append(ClausesD1,ClausesD2,Clauses).


/*sphere_expansion((Head :- Body),Clauses) :-
% definite clause for db facts
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  ((Head:-Body) \= ((sphere_expansion(_,_)) :- _ )),
  Head=db(Head1),!,
  Clauses=(Head1 :- Body).
*/
sphere_expansion((Head :- Body),Clauses) :-
% definite clause with depth_bound
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
  ((Head:-Body) \= ((sphere_expansion(_,_)) :- _ )),!,
  double_head([Head],[HeadD]),
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  process_body_db(BodyListD1,BDD,BDDAnd,DB,[],_Vars,BodyListD12,Env,M),
  process_body_db(BodyListD2,BDD,BDDAnd,DB,[],_VarsD,BodyListD22,Env,M),
  append([onec(Env,BDD)],BodyListD12,BodyListD13),
  append([onec(Env,BDD)],BodyListD22,BodyListD23),
  append(BodyListD13,[sphere:sphere_check_inconsistency(M,Head,Env,BDDAnd)],BodyListD14),
  append(BodyListD23,[sphere:sphere_neg_H(M,_H12,Head,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(M,Head,Env,BDDAndD)],BodyListD24),
  list2and([DBH>=1,DB is DBH-1|BodyListD14],BodyD12),
  list2and([DBH>=1,DB is DBH-1|BodyListD24],BodyD22),
  to_table(M,[Head:_],TabDirD1,[Head1:_]),
  to_table(M,[HeadD:_],TabDirD2,[HeadD1:_]),
  add_bdd_arg_db(Head1,Env,BDDAnd,DBH,M,Head2),
  add_bdd_arg_db(HeadD1,Env,BDDAndD,DBH,M,HeadD2),
  assert_head_pred(M,Head),
  append(TabDirD1,[(Head2 :- BodyD12)],ClausesD1),
  append(TabDirD2,[(HeadD2 :- BodyD22)],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion((Head :- Body),Clauses) :-
%  writeln((Head:-Body)),
 % trace,
% definite clause without DB
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  ((Head:-Body) \= ((sphere_expansion(_,_)) :- _ )),!,
  double_head([Head],[HeadD]),
  list2and(BodyList, Body),
  double_body(BodyList,BodyListD1,BodyListD2),
  process_body(BodyListD1,BDD,BDDAnd,[],_Vars,BodyListD12,Env,M),
  process_body(BodyListD2,BDD,BDDAnd,[],_VarsD,BodyListD22,Env,M),
  append([onec(Env,BDD)],BodyListD12,BodyListD13),
  append([onec(Env,BDD)],BodyListD22,BodyListD23),
  append(BodyListD13,[sphere:sphere_check_inconsistency(M,Head,Env,BDDAnd)],BodyListD14),
  append(BodyListD23,[sphere:sphere_neg_H(M,_H12,Head,Env,BDDAnd,BDDN),andc(Env,BDDAnd,BDDN,BDDAndD),sphere:sphere_check_inconsistency(M,Head,Env,BDDAndD)],BodyListD24),
  list2and(BodyListD14,BodyD12),
  list2and(BodyListD24,BodyD22),
  to_table(M,[Head:_],TabDirD1,[Head1:_]),
  to_table(M,[HeadD:_],TabDirD2,[HeadD1:_]),
  add_bdd_arg(Head1,Env,BDDAnd,M,Head2),
  add_bdd_arg(HeadD1,Env,BDDAndD,M,HeadD2),
  assert_head_pred(M,Head),
  append(TabDirD1,[(Head2 :- BodyD12)],ClausesD1),
  append(TabDirD2,[(HeadD2 :- BodyD22)],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Axiom,[]):-
  Axiom =.. [P|Args],
  owl2_model:is_axiom(P),
  owl2_model:create_and_assert_axioms(P,Args).

sphere_expansion(Head,
  [rule_by_num(R,HeadListD1,[],VC1),rule_by_num(R,HeadListD2,[],VC1)|Clauses]) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
% disjunctive FACT with more than one head atom e db
  Head=(_;_), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadListD1),
  process_head(HeadListOrD, HeadListD2),
  term_variables(HeadListD1,VC),
  get_next_rule_number(M,R),
  get_probs(HeadListD1,Probs),
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  to_table(M,HeadListD1,TabDirD1,HeadListD11),
  to_table(M,HeadListD2,TabDirD2,HeadListD21),
  generate_rules_fact_db(HeadListD11,_Env,VC1,R,Probs,0,ClausesD10,M,false),
  generate_rules_fact_db(HeadListD21,_EnvD1,VC1,R,Probs,0,ClausesD20,M,true),
  append(TabDirD1,ClausesD10,ClausesD1),
  append(TabDirD2,ClausesD20,ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Head,[rule_by_num(R,HeadListD1,[],VC1),rule_by_num(R,HeadListD2,[],VC1)|Clauses]) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
% disjunctive fact with more than one head atom senza db
  Head=(_;_), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadListD1),
  process_head(HeadListOrD, HeadListD2),
  term_variables(HeadListD1,VC),
  get_next_rule_number(M,R),
  get_probs(HeadListD1,Probs), %**** test single_var
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  to_table(M,HeadListD1,TabDirD1,HeadListD11),
  to_table(M,HeadListD2,TabDirD2,HeadListD21),
  generate_rules_fact(HeadListD11,_Env,VC1,R,Probs,0,ClausesD10,M,false),
  generate_rules_fact(HeadListD21,_EnvD1,VC1,R,Probs,0,ClausesD20,M,true),
  append(TabDirD1,ClausesD10,ClausesD1),
  append(TabDirD2,ClausesD20,ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Head,[]) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
% disjunctive fact with a single head atom with prob. 0
  (Head \= ((sphere_expansion(_,_)) :- _ )),
  (Head = (_:P); Head = (P::_)),
  ground(P),
  P=:=0.0, !.

sphere_expansion(Head,Clauses) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
% disjunctive fact with a single head atom with prob = 1 and depth bound
  (Head \= ((sphere_expansion(_,_)) :- _ )),
  (Head = (H:P); Head = (P::H)),
  ground(P),
  P=:=1.0, !,
  double_head([Head],[HeadD]),
  list2and([onec(Env,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)],Body1),
  list2and([onec(Env,BDDOne),sphere:sphere_neg_H(M,_HD11,H,Env,BDDOne,BDDN),andc(Env,BDDOne,BDDN,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)],BodyD1),
  to_table(M,[Head:_],TabDirD1,[H1:_]),
  to_table(M,[HeadD:_],TabDirD2,[HD1:_]),
  add_bdd_arg_db(H1,Env,BDD,_DB,M,Head1),
  add_bdd_arg_db(HD1,Env,BDD,_DBD,M,HeadD1),
  assert_head_pred(M,H),
  append(TabDirD1,[(Head1 :- Body1)],ClausesD1),
  append(TabDirD2,[(HeadD1 :- BodyD1)],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Head,Clauses) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
% disjunctive fact with a single head atom with prob = 1 without depth bound
  (Head \= ((sphere_expansion(_,_)) :- _ )),
  (Head = (H:P);Head =(P::H)),
  ground(P),
  P=:=1.0, !,
  double_head([Head],[HeadD]),
  list2and([onec(Env,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)],Body1),
  list2and([onec(Env,BDDOne),sphere:sphere_neg_H(M,_HD11,H,Env,BDDOne,BDDN),andc(Env,BDDOne,BDDN,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)],BodyD1),
  to_table(M,[Head:_],TabDirD1,[H1:_]),
  to_table(M,[HeadD:_],TabDirD2,[HD1:_]),
  add_bdd_arg(H1,Env,BDD,M,Head1),
  add_bdd_arg(HD1,Env,BDD,M,HeadD1),
  assert_head_pred(M,H),
  append(TabDirD1,[(Head1 :- Body1)],ClausesD1),
  append(TabDirD2,[(HeadD1 :- BodyD1)],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Head,[rule_by_num(R,HeadList,[],VC1)|Clauses]) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
% disjunctive fact with a single head atom and generic prob with depth bound
  (Head \= ((sphere_expansion(_,_)) :- _ )),
  (Head=(H:_);Head=(_::H)), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),
  process_head(HeadListOrD, HeadListD),
  term_variables(HeadList,VC),
  get_next_rule_number(M,R),
  get_probs(HeadList,Probs),
  to_table(M,HeadList,TabDirD1,[H1:_]),
  to_table(M,HeadListD,TabDirD2,[HD1:_]),
  add_bdd_arg_db(H1,Env,BDD,_DB,M,Head1),
  add_bdd_arg_db(HD1,Env,BDD,_DBD,M,HeadD1),
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  Clauses0=[(Head1:-(get_var_n(M,Env,R,VC1,Probs,V),equalityc(Env,V,0,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)))],
  ClausesD0=[(HeadD1:-(get_var_n(M,Env,R,VC1,Probs,V),equalityc(Env,V,0,BDDH),sphere:sphere_neg_H(M,_HD11,H,Env,BDDH,BDDN),andc(Env,BDDH,BDDN,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)))],
  append(TabDirD1,Clauses0,ClausesD1),
  append(TabDirD2,ClausesD0,ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Head,[rule_by_num(R,HeadList,[],VC1)|Clauses]) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
% disjunctive fact with a single head atom and generic prob without depth bound
  (Head \= ((sphere_expansion(_,_)) :- _ )),
  (Head=(H:_);Head=(_::H)), !,
  list2or(HeadListOr, Head),
  double_head(HeadListOr,HeadListOrD),
  process_head(HeadListOr, HeadList),
  process_head(HeadListOrD, HeadListD),
  term_variables(HeadList,VC),
  get_next_rule_number(M,R),
  get_probs(HeadList,Probs),
  to_table(M,HeadList,TabDirD1,[H1:_]),
  to_table(M,HeadListD,TabDirD2,[HD1:_]),
  % write('headlist: '), writeln(HeadList),
  % write('h1: '), writeln(H1),
  add_bdd_arg(H1,Env,BDD,M,Head1),%***test single_var
  add_bdd_arg(HD1,Env,BDD,M,HeadD1),%***test single_var
  % write('head1: '), writeln(Head1),
  % write('vc: '), writeln(VC),
  (M:local_sphere_setting(single_var,true)->
    VC1 = []
  ;
    VC1 = VC
  ),
  Clauses0=[(Head1:-(get_var_n(M,Env,R,VC1,Probs,V),equalityc(Env,V,0,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)))],
  ClausesD0=[(HeadD1:-(get_var_n(M,Env,R,VC1,Probs,V),equalityc(Env,V,0,BDDH),sphere:sphere_neg_H(M,_HD11,H,Env,BDDH,BDDN),andc(Env,BDDH,BDDN,BDD),sphere:sphere_check_inconsistency(M,H,Env,BDD)))],
  append(TabDirD1,Clauses0,ClausesD1),
  append(TabDirD2,ClausesD0,ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion((:- set_sphere(P,V)), []) :-!,
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  set_sphere(P,V).

sphere_expansion((:- set_sw(A,B)), []) :-!,
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  set_sw(M:A,B).


sphere_expansion(Head, Clauses) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
  M:local_sphere_setting(depth_bound,true),
% definite fact with db
  (Head \= ((sphere_expansion(_,_) ):- _ )),
  (Head\= end_of_file),!,
  double_head([Head],[HeadD]),
  to_table(M,[Head:_],TabDirD1,[Head1:_]),
  to_table(M,[HeadD:_],TabDirD2,[HeadD1:_]),
  add_bdd_arg_db(Head1,Env,One,_DB,M,Head2),
  add_bdd_arg_db(HeadD1,Env,One,_DBD,M,HeadD2),
  assert_head_pred(M,Head),
  append(TabDirD1,[(Head2:-(onec(Env,One),sphere:sphere_check_inconsistency(M,Head,Env,One)))],ClausesD1),
  append(TabDirD2,[(HeadD2:-(onec(Env,BDDOne),sphere:sphere_neg_H(M,_HD11,Head,Env,BDDOne,BDDN),andc(Env,BDDOne,BDDN,One),sphere:sphere_check_inconsistency(M,Head,Env,One)))],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

sphere_expansion(Head, Clauses) :-
  prolog_load_context(module, M),sphere_input_mod(M),M:sphere_on,
% definite fact without db
  (Head \= ((sphere_expansion(_,_) ):- _ )),
  (Head\= end_of_file),
  double_head([Head],[HeadD]),
  to_table(M,[Head:_],TabDirD1,[Head1:_]),
  to_table(M,[HeadD:_],TabDirD2,[HeadD1:_]),
  add_bdd_arg(Head1,Env,One,M,Head2),
  add_bdd_arg(HeadD1,Env,One,M,HeadD2),
  assert_head_pred(M,Head),
  append(TabDirD1,[(Head2:-(onec(Env,One),sphere:sphere_check_inconsistency(M,Head,Env,One)))],ClausesD1),
  append(TabDirD2,[(HeadD2:-(onec(Env,BDDOne),sphere:sphere_neg_H(M,_HD11,Head,Env,BDDOne,BDDN),andc(Env,BDDOne,BDDN,One),sphere:sphere_check_inconsistency(M,Head,Env,One)))],ClausesD2),
  append(ClausesD1,ClausesD2,Clauses).

/**
 * begin_lpad_pred is det
 *
 * Initializes LPAD loading.
 */
begin_lpad_pred:-
  assert(sphere_input_mod(user)),
  assert(user:sphere_on).

/**
 * end_lpad_pred is det
 *
 * Terminates the cplint inference module.
 */
end_lpad_pred:-
  retractall(sphere_input_mod(_)),
  retractall(user:sphere_on).

list2or([],true):-!.

list2or([X],X):-
    X\=;(_,_),!.

list2or([H|T],(H ; Ta)):-!,
    list2or(T,Ta).


list2and([],true):-!.

list2and([X],X):-
    X\=(_,_),!.

list2and([H|T],(H,Ta)):-!,
    list2and(T,Ta).

transform(H,H1):-
  H=..[prob|Args],
  H1=..[prob_meta|Args].

builtin(average(_L,_Av)) :- !.
builtin(G) :-
  swi_builtin(G).


:- multifile sandbox:safe_meta/2.

sandbox:safe_meta(sphere:prob(_,_), []).
sandbox:safe_meta(sphere:prob_meta(_,_), []).
sandbox:safe_meta(sphere:bdd_dot_file(_,_,_), []).
sandbox:safe_meta(sphere:bdd_dot_string(_,_,_), []).
sandbox:safe_meta(sphere:get_bdd(_,_,_,_), []).
sandbox:safe_meta(sphere:set_sphere(_,_),[]).
sandbox:safe_meta(sphere:setting_sphere(_,_),[]).




:- license(artisticv2).

:- thread_local sphere_file/1.

user:term_expansion(:-sphere, Clauses) :-!, %look sphere_old
  trill:add_kb_prefix('disponte','https://sites.google.com/a/unife.it/ml/disponte#'),
  prolog_load_context(source, Source),
  asserta(sphere_file(Source)),
  prolog_load_context(module, M),
  retractall(M:local_sphere_setting(_,_)),
  findall(local_sphere_setting(P,V),default_setting_sphere(P,V),L),
  assert_all(L,M,_),
  assert(sphere_input_mod(M)),
  retractall(M:rule_n(_)),
  retractall(M:goal_n(_)),
  retractall(M:lp_axioms_for_oracle(_,_)),
  assert(M:rule_n(0)),
  assert(M:goal_n(0)),
  assert(M:lp_axioms_for_oracle([],[])),
  M:(dynamic na/2, v/3, av/3, query_rule/4, rule_by_num/4, dec/3,
    zero_clauses/1, sphere_on/0, tabled/1, '$cons'/2, inconsistency_warning/0),
  retractall(M:query_rule(_,_,_,_)),
  retractall(M:na(_,_)),
  style_check(-discontiguous),
  process_body([\+ '$cons'],BDD,BDDAnd,[],_Vars,BodyList2,Env,M),
  append([onec(Env,BDD)],BodyList2,BodyList3),
  list2and(BodyList3,Body2),
  to_table(M,['$constraints':_],TabDir,[Head1:_]),
  to_table(M,['$cons':_],TabDirCons,_),
  add_bdd_arg(Head1,Env,BDDAnd,M,Head2),
  append([TabDir,TabDirCons,[(Head2 :- Body2)]],Clauses).

user:term_expansion(end_of_file, C) :- 
  sphere_file(Source),
  prolog_load_context(source, Source),
  retractall(sphere_file(Source)),
  prolog_load_context(module, M),
  sphere_input_mod(M),!,
  retractall(sphere_input_mod(M)),
  findall(LZ,M:zero_clauses(LZ),L),
  %findall(LZ,M:zero_clauses(LZ),L0),
  %append(L0,L),
  retractall(M:zero_clauses(_)),
  retractall(M:tabled(_)),
  append(L,[(:- style_check(+discontiguous)),end_of_file],C).

user:term_expansion(In, Out) :-
   \+ current_prolog_flag(xref, true),
   sphere_file(Source),
   prolog_load_context(source, Source),
   sphere_expansion(In, Out).

