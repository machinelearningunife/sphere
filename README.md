spHERE
=====

spHERE ("Hybrid knowlEdge bases REasoner") is an algorithm for reasoning on Probabilistic Hybrid Knowledge Bases
 
This is implemented for SWI-Prolog (http://www.swi-prolog.org/).

Installation
------------

spHERE need cplint to be installed. cplint can be installed with `pack_install/1`.

    $ swipl
    ?- pack_install(cplint).

The pack uses a foreign library and contains the library binaries for 32 and 64 bits Linux and 32 and 64 bits Windows. 
If you want to recompile the foreign library you can use

    ?- pack_rebuild(bddem).

On 32 and 64 bits Linux this should work out of the box. On 64 bits Windows the library must be rebuilt by hand. The procedure for building it using MSYS2 can be found in the `bddem`'s README

Example of use
---------------

    $ cd examples
    $ swipl Example_1.pl
    ?- query(p(b)).
    true.
