:- use_module('../sPHeRE/sphere.pl').

:- sphere.

:- begin_phkb.

subClassOf(someValuesFrom(mutation,'owl:Thing'),mutated).
classAssertion(someValuesFrom(mutation,'owl:Thing'),t).
annotationAssertion('disponte:probability',classAssertion(someValuesFrom(mutation,'owl:Thing'),t),literal('0.8')).



spillover_count(X, s(Y )) : 0.6 :- virus(X), mutated(X), spillover_count(X,Y).
spillover_count(X, 0) :- virus(X).
virus(t).


:- end_phkb.
 
