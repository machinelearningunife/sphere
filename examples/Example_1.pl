:- use_module('../sPHeRE/sphere.pl').

:- sphere.

:- begin_phkb.

subClassOf(c,d).
subClassOf(intersectionOf([c,e]),p).
classAssertion(c,b).


p(X):- o(X),\+d(X).
%d(X):-o(X).
e(X):-o(X).
o(a).
o(b).
p(b):-e(a).


:- end_phkb.
