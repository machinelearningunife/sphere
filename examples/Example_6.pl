:- use_module('../sPHeRE/sphere.pl').

:- sphere.

:- begin_phkb.

a(X):- \+ b(X).
b(X):- \+ a(X).


classAssertion(complementOf(a),i1).



:- end_phkb.
