:- use_module('../sPHeRE/sphere.pl').

:- sphere.

:- begin_phkb.


subClassOf(c,complementOf(h)).
h(a).
c(a):- \+ h(a).

:- end_phkb.
