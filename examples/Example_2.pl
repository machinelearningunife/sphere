:- use_module('../sPHeRE/sphere.pl').

:- sphere.

:- begin_phkb.

subClassOf(loner,ineffective).
% subClassOf(someValuesFrom(connected,influencer),social).
subClassOf(someValuesFrom(connected,'Thing'),social).

loner(X) :- \+ social(X),person(X).
discount(X) :- \+ ineffective(X),person(X).

connected(X,Y):- edge(X,Y).
connected(X,Y):- connected(X,Z),edge(Z,Y).

edge(bill,stephanie).
influencer(stephanie).

person(bill).
person(stephanie).


:- end_phkb.
